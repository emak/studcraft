from google.appengine.ext import db

class Team(db.Model):
   """Models a StarCraft II team/clan"""
   name = db.StringProperty()
   score = db.FloatProperty()
   votecount = db.IntegerProperty()

class Race(db.Model):
   """Models a StarCraft II race (ZPTR)"""
   name = db.StringProperty()
   score = db.FloatProperty()
   votecount = db.IntegerProperty()

class SpamHash(db.Model):
   """Used to ensure users can't choose who they vote on"""
   player1 = db.StringProperty()
   player2 = db.StringProperty()
   secret = db.StringProperty()

class Player(db.Model):
   """Models a StarCraft II player"""
   handle = db.StringProperty()
   name = db.StringProperty()
   team = db.ReferenceProperty(Team)
   race = db.ReferenceProperty(Race)
   nation = db.StringProperty()
   photo = db.StringProperty()
   score = db.FloatProperty()
   votecount = db.IntegerProperty()

class Statistic(db.Model):
   votecount = db.IntegerProperty()
