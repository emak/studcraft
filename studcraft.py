import os
import logging
import random
import math
import base64

from google.appengine.ext import db
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.ext.webapp import template

from models import *

class Initialise(webapp.RequestHandler):
   def get(self):
      self.response.headers['Content-Type'] = 'text/plain'
      path = os.path.join(os.path.dirname(__file__), 'players.csv')
      statistic = Statistic(votecount=0)
      statistic.put()
      for line in open(path):
         fields = line[:-1].split(',')
         self.response.out.write("Fields: %s\n" % str(fields))
         (handle, name, teamName, nation, raceName, photo) = fields
         score = 1000.0
         team = None
         if teamName != '':
            team = Team.all().filter("name = ", teamName).get()
            if team is None:
               team = Team(name=teamName, score=1000.0, votecount=0)
               team.put()
         race = Race.all().filter("name = ", raceName).get()
         if race is None:
            race = Race(name=raceName, score=1000.0, votecount=0)
            race.put()
         player = Player(handle=handle, team=team, name=unicode(name), nation=nation, race=race, photo=photo, score=score, votecount=0)
         player.put()

class NewsPage(webapp.RequestHandler):
   def get(self):
      self.response.headers['Content-Type'] = 'text/html'
      path = os.path.join(os.path.dirname(__file__), 'news.html')
      self.response.out.write(template.render(path, {'votecount' : Statistic.all().fetch(1)[0].votecount}))



class PlayersPage(webapp.RequestHandler):
   def get(self):
      players = Player.gql("ORDER BY score DESC LIMIT 50").fetch(50)
      self.response.headers['Content-Type'] = 'text/html'
      path = os.path.join(os.path.dirname(__file__), 'players.html')
      self.response.out.write(template.render(path, {'players' : players , 'first' : players[0], 'second' : players[1], 'votecount' : Statistic.all().fetch(1)[0].votecount}))

class RacesPage(webapp.RequestHandler):
   def get(self):
      races = Race.gql("ORDER BY score DESC LIMIT 50")
      self.response.headers['Content-Type'] = 'text/html'
      path = os.path.join(os.path.dirname(__file__), 'races.html')
      self.response.out.write(template.render(path, {'races' : races, 'votecount' : Statistic.all().fetch(1)[0].votecount}))

class TeamsPage(webapp.RequestHandler):
   def get(self):
      teams = Team.gql("ORDER BY score DESC LIMIT 50")
      self.response.headers['Content-Type'] = 'text/html'
      path = os.path.join(os.path.dirname(__file__), 'teams.html')
      self.response.out.write(template.render(path, {'teams' : teams,  'votecount' : Statistic.all().fetch(1)[0].votecount}))

class MainPage(webapp.RequestHandler):
   def updateScore(self, winner, loser):
      if winner is not None and loser is not None and winner.key() != loser.key(): # thought it was odd that random was winning (lol)
         K = 20
         winnerQ = math.pow(10, winner.score/400.0)
         loserQ  = math.pow(10, loser.score/400.0)
         winnerE = winnerQ/(winnerQ+loserQ)
         winnerS = 1.0
         loserE  = loserQ/(winnerQ+loserQ)
         loserS  = 0.0
         winner.score = winner.score + K*(winnerS - winnerE)
         loser.score  = loser.score + K*(loserS - loserE)
         logging.info('Winner is %s', winner.name)
         logging.info('Updated %s to have score %f', winner.name, winner.score)
         logging.info('Updated %s to have score %f', loser.name, loser.score)
         winner.votecount = winner.votecount + 1
         winner.put()
         loser.votecount = loser.votecount + 1
         loser.put()

   def processForm(self):
      spamhash = self.request.get('spamhash', None)
      player1ID = self.request.get('player1', None)
      player2ID = self.request.get('player2', None)
      winnerID = self.request.get('winner', None)
      if spamhash is not None and player1ID is not None and player2ID is not None and winnerID in ['0','1']:
         try:
            player1 = Player.get(player1ID)
            player2 = Player.get(player2ID)
         except db.BadKeyError:
            player1 = None
            player2 = None
         if player1 is not None and player2 is not None:
            matchingSpamHash = SpamHash.gql("WHERE player1 = :1 AND player2 = :2 AND secret = :3", player1ID, player2ID, spamhash).get()
            if matchingSpamHash is not None:
               matchingSpamHash.delete()
               if winnerID == '0':
                  winner = player1
                  loser = player2
               else:
                  winner = player2
                  loser = player1
               statistic = Statistic.all().fetch(1)[0]
               statistic.votecount = statistic.votecount + 1
               statistic.put()
               self.updateScore(winner, loser)
               self.updateScore(winner.team, loser.team)
               self.updateScore(winner.race, loser.race)
               return True
         path = os.path.join(os.path.dirname(__file__), 'fuckyou.html')
         self.response.headers['Content-Type'] = 'text/html'
         self.response.out.write(template.render(path, {'votecount' : Statistic.all().fetch(1)[0].votecount}))

         return False
      else:
         return True

   def showVotePage(self):
      allPlayers = Player.all(keys_only=True)
      numPlayers = allPlayers.count()
      if (numPlayers < 2):
         self.error(500)
         return
      player1offset = random.randint(0, numPlayers-1)
      player2offset = player1offset
      while (player1offset == player2offset):
         player2offset = random.randint(0, numPlayers-1)
      player1 = Player.all().fetch(limit=1, offset=player1offset)[0]
      player2 = Player.all().fetch(limit=1, offset=player2offset)[0]
      path = os.path.join(os.path.dirname(__file__), 'vote.html')
      spamhash = SpamHash(player1=unicode(player1.key()),
                          player2=unicode(player2.key()),
                          secret=base64.b64encode(os.urandom(8), "*!"))
      spamhash.put()
      self.response.headers['Content-Type'] = 'text/html'
      self.response.out.write(template.render(path, 
         {'player1' : player1,
          'player2': player2,
          'spamhash' : spamhash,
          'votecount' : Statistic.all().fetch(1)[0].votecount}))

   def post(self):
      if self.processForm():
         self.redirect("/")

   def get(self):
      self.showVotePage()

application = webapp.WSGIApplication(
      [
       ('/initialise', Initialise),
       ('/players', PlayersPage),
       ('/races', RacesPage),
       ('/teams', TeamsPage),
       ('/news', NewsPage),
       ('/', MainPage)
      ],
      debug=True)


def main():
   run_wsgi_app(application)

if __name__ == "__main__":
   main()

